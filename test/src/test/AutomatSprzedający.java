package test;

public class AutomatSprzedaj�cy {
	
	Stan stanBrakGum = new Stan(){

		@Override
		public void w��Monet�() {
			// TODO Auto-generated method stub
			System.out.println("abc");
		}

		@Override
		public void zwr��Monet�() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void przekr��Ga�k�() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void wydaj() {
			// TODO Auto-generated method stub
			
		}
		
	};
	Stan stanNieMaMonety;
	Stan stanJestMoneta;
	Stan stanGumaSprzedana;
	Stan stanWygrana;
	
	Stan stan = stanBrakGum;
	int liczba = 0;
	
	public AutomatSprzedaj�cy(int liczbaGum, Stan stanBrakGum, Stan stanNieMaMonety, Stan stanJestMoneta, Stan stanGumaSprzedana) {
		this.stanBrakGum = stanBrakGum;
		this.stanNieMaMonety = stanNieMaMonety;
		this.stanJestMoneta = stanJestMoneta;
		this.stanGumaSprzedana = stanGumaSprzedana;
		
		this.liczba = liczbaGum;
		if (liczbaGum > 0) {
			stan = stanNieMaMonety;
		}	
	}

	public void w��Monet�() {
		stan.w��Monet�();
	}
	
	public void zwr��Monet�() {
		stan.zwr��Monet�();
	}
	
	public void przekr��Ga�k�() {	
		stan.przekr��Ga�k�();
		stan.wydaj();		
	}
	
	void ustawStan(Stan stan) {
		this.stan = stan;
	}
	
	void zwolnijGum�() {
		System.out.println("Wypada guma...");
		if (liczba != 0) {
			liczba = liczba - 1;
		}
	}

	public Stan getStanBrakGum() {
		return stanBrakGum;
	}

	public Stan getStanNieMaMonety() {
		return stanNieMaMonety;
	}

	public Stan getStanJestMoneta() {
		return stanJestMoneta;
	}

	public Stan getStanGumaSprzedana() {
		return stanGumaSprzedana;
	}

	public Stan getStan() {
		return stan;
	}

	public int getLiczba() {
		return liczba;
	}

	public void setStanBrakGum(Stan stanBrakGum) {
		this.stanBrakGum = stanBrakGum;
	}

	public void setStanNieMaMonety(Stan stanNieMaMonety) {
		this.stanNieMaMonety = stanNieMaMonety;
	}

	public void setStanJestMoneta(Stan stanJestMoneta) {
		this.stanJestMoneta = stanJestMoneta;
	}

	public void setStanGumaSprzedana(Stan stanGumaSprzedana) {
		this.stanGumaSprzedana = stanGumaSprzedana;
	}

	public void setStan(Stan stan) {
		this.stan = stan;
	}

	public void setLiczba(int liczba) {
		this.liczba = liczba;
	}

	public Stan getStanWygrana() {
		return stanWygrana;
	}

	public void setStanWygrana(Stan stanWygrana) {
		this.stanWygrana = stanWygrana;
	}
	
	

}
