package main;

import java.io.IOException;
import java.util.Scanner;

public class Player {

	private Figure[][] board;
	private Game game;
	private String color;
	private String myFigure;
	private String opFigure;
	private int warriors = 0;

	public Player(Figure[][] board, Game game, String color) {
		this.board = board;
		this.game = game;
		this.color = color;
	}

	public void move() throws IOException {
		setUpSides();
		System.out.println("");
		System.out.println("Player " + color + " turn: ");
		String[] temp = inputValidation().split(" -> ");
		String start = temp[0];
		String destination = temp[1];
		int startIndex[] = translator(start);
		int destIndex[] = translator(destination);

		System.out.println("start:" + startIndex[0] + " " + startIndex[1]);
		System.out.println("destination:" + destIndex[0] + " " + destIndex[1]);

		if (board[startIndex[0]][startIndex[1]].state.equals(myFigure)) {
			if ((startIndex[0] - destIndex[0] == 1 && color == "white")
					|| (startIndex[0] - destIndex[0] == -1 && color == "black")) {
				if (startIndex[1] >= 0 && startIndex[1] < 8) {
					if (((Math.abs(destIndex[1] - startIndex[1]) == 1) || (destIndex[1] - startIndex[1] == 0))
							&& (board[destIndex[0]][destIndex[1]].state == "n")) {
						(board[destIndex[0]][destIndex[1]]).state = myFigure;
						board[startIndex[0]][startIndex[1]].state = "n";
						if (destIndex[0] == 0) {
							System.out.println("Player White won!");
							game.setWin(true);
						}
						if (destIndex[0] == 7) {
							System.out.println("Player Black won!");
							game.setWin(true);
						}

					} else if ((Math.abs(destIndex[1] - startIndex[1]) == 1)
							&& (board[destIndex[0]][destIndex[1]].state == opFigure)) {
						int warriors = game.getEnemyWarriors();
						game.setEnemyWarriors(warriors - 1);
						board[destIndex[0]][destIndex[1]].state = myFigure;
						board[startIndex[0]][startIndex[1]].state = "n";
						if (destIndex[0] == 0) {
							System.out.println("Player White won!");
							game.setWin(true);
						}

					} else {
						System.out.println("You can't do that move. Try again. 1");
						move();
					}
				} else {
					System.out.println("You can't do that move. Try again. 2");
					move();
				}
			} else {
				System.out.println("You can't do that move. Try again. 3");
				move();
			}
		}
		if (color == "white") {
			game.setMyWarriors(warriors);
		} else {
			game.setEnemyWarriors(warriors);
		}
		game.setBoard(board);
	}

	public void setUpSides() {
		if (color == "white") {
			myFigure = "w";
			opFigure = "b";
			warriors = game.getMyWarriors();
			
		} else {
			myFigure = "b";
			opFigure = "w";
			warriors = game.getEnemyWarriors();
		}
	}

	public int[] translator(String str) {
		String tmp[] = str.split("");
		int tmpInt[] = new int[2];
		switch (tmp[0]) {
		case "a":
			tmpInt[1] = 0;
			break;
		case "b":
			tmpInt[1] = 1;
			break;
		case "c":
			tmpInt[1] = 2;
			break;
		case "d":
			tmpInt[1] = 3;
			break;
		case "e":
			tmpInt[1] = 4;
			break;
		case "f":
			tmpInt[1] = 5;
			break;
		case "g":
			tmpInt[1] = 6;
			break;
		case "h":
			tmpInt[1] = 7;
			break;
		}
		switch (tmp[1]) {
		case "0":
			tmpInt[0] = 8;
			break;
		case "1":
			tmpInt[0] = 7;
			break;
		case "2":
			tmpInt[0] = 6;
			break;
		case "3":
			tmpInt[0] = 5;
			break;
		case "4":
			tmpInt[0] = 4;
			break;
		case "5":
			tmpInt[0] = 3;
			break;
		case "6":
			tmpInt[0] = 2;
			break;
		case "7":
			tmpInt[0] = 1;
			break;
		}

		return tmpInt;
	}

	public boolean isNumeric(String s) {
		return s != null && s.matches("[-+]?\\d*\\.?\\d+");
	}

	public String inputValidation() {
		String result = "";
		String lastMove = "";
		String wholeMove = "";
		boolean properResult = true;
		while (properResult) {

			Scanner sc = new Scanner(System.in);
			wholeMove = sc.nextLine();
			String temp[] = wholeMove.split("");
			if (wholeMove != lastMove) {
				if (temp.length == 8) {
					if (Character.isLetter(temp[0].charAt(0)) && isNumeric(temp[1]) && temp[2].equals(" ")
							&& temp[3].equals("-") && temp[4].equals(">") && temp[5].equals(" ")
							&& Character.isLetter(temp[6].charAt(0)) && isNumeric(temp[7])) {
						result = wholeMove;
						properResult = false;
					} else {
						System.out.println("Remember: use syntax like this: c4 -> c5 ! Try again.");
					}
				} else {
					System.out.println("Remember: use space between the arrow like this: b2 -> b3 ! Try again.");

				}
			} else {
				System.out.println("Remember: use space between the arrow like this: b2 -> b3 ! Try again.");
			}
		}
		lastMove = wholeMove;
		return result;
	}
}
