package main;

import java.io.IOException;
import java.util.Scanner;

import enemy.Enemy;
import enemy.Random;

public class Main {

	private static int style;
	
	public static void main(String[] args) throws IOException {
		
		while(true){
			style = menu();
			if (style == 4) {
				break;
			}
			Game game = new Game(style);
			game.startGame();
		}
		System.out.println("Thanks!");
	}

	private static int menu() {
		System.out.println(" Click number to start game: ");
		System.out.println(" 1. PLAYER VS. PLAYER");
		System.out.println(" 2. PLAYER VS. RANDOM AI ");
		System.out.println(" 3. PLAYER VS. KILLER AI ");
		System.out.println(" 4. QUIT ");
		
		Scanner sc = new Scanner(System.in);
		int number = sc.nextInt(); 
		return number;
	}

}
