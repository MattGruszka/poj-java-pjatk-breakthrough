package enemy;

import java.util.ArrayList;

import main.Figure;
import main.Game;

public class Enemy {

	public Move moveType;
	
	public void move() {
		moveType.move();
		
	}
	
	public void setMoveType(Move newMoveType) {
		moveType = newMoveType;
		
	}
	
	
}
