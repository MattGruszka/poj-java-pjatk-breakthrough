package enemy;


public interface Move {

	 void move();
}


class RandomAI implements Move {
	
	@Override
	public void move() {
		//pick random 
		// place for making MOVE a special behavior typical for RandomAI
		System.out.println("AI MAKING RANDOM MOVE");
	}
}

class KillAI implements Move {

	@Override
	public void move() {
		//first - kill, else -random
		// place for making MOVE a special behavior typical for KillAI
		System.out.println("AI KILL IF POSSIBLE");
	}

}
